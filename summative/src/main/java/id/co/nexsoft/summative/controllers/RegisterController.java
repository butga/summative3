package id.co.nexsoft.summative.controllers;

import id.co.nexsoft.summative.entities.User;
import id.co.nexsoft.summative.services.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RegisterController {

    @Autowired
    private RegisterService registerService;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegister() {
        return "register.jsp";
    }

    @RequestMapping(value = "/save-user", method = RequestMethod.POST)
    public String sendAddress(@ModelAttribute User user, Model model) {

        if (registerService.checkRetypePassword(user)) {
            registerService.register(user);
            return "welcome.jsp";
        } else {
            model.addAttribute("message", "Gagal Register, password dan retype password tidak sesuai");
            return "register.jsp";
        }

    }

}
