<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 21/08/2021
  Time: 14:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Landing Page</title>

    <!-- css -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <!-- timepicker -->
    <link rel="stylesheet" href="./styles/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
</head>

<body>
<div class="container">

    <%--error message--%>
    <c:if test = "${message != null}">
        <div class="alert alert-danger mt-2" role="alert">
                ${message}
        </div>
    </c:if>

    <div class="mb-4 mt-4">
        <h2 class="text-center">Personal Information</h2>
    </div>
    <div class="col-8 offset-sm-2">
        <form method="POST" modelAttribute = "user" action="/save-user">
            <div class="form-group row">
                <label for="first-name" class="col-sm-3 col-form-label">First Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="first-name" name="firstName">
                </div>
            </div>

            <div class="form-group row">
                <label for="last-name" class="col-sm-3 col-form-label">Last Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="last-name" name="lastName">
                </div>
            </div>

            <div class="form-group row">
                <label for="last-name" class="col-sm-3 col-form-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="email" name="email" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="phone" class="col-sm-3 col-form-label">Phone</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="phone" name="phone" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="datetimepicker1" class="col-sm-3 col-form-label">Birth Date</label>
                <section class="col-sm-9">
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control" name = "birthDate"/>
                            <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                        </div>
                    </div>
                </section>

            </div>

            <div class="form-group row">
                <label for="gender" class="col-sm-3 col-form-label">Gender</label>
                <div class="col-sm-9 mt-1">
                    <select id="gender" name="gender" class="form-select">
                        <option selected value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-sm-3 col-form-label">Password</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="password" name="password" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="retype-password" class="col-sm-3 col-form-label">Retype Password</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="retype-password" name="retypePassword" required>
                </div>
            </div>


            <button type="submit" class="btn btn-primary offset-sm-3">Register</button>
            <button type="button" class="btn btn-secondary" onClick="clearAllInput();">Reset</button>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('#datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });
    });
</script>

<script>
    function clearAllInput() {
        document.getElementById("first-name").value = '';
        document.getElementById("last-name").value = '';
        document.getElementById("email").value = '';
        document.getElementById("phone").value = '';
        document.getElementById("password").value = '';
        document.getElementById("retype-password").value = '';
        document.getElementById("datetimepicker1").selectedIndex = 0;
        document.getElementById("gender").selectedIndex = 0;
    }
</script>
</body>

</html>